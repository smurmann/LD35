﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreController : MonoBehaviour {
	public Text scoreText;
	public Text multiplierText;

	long score;
	int multiplier = 1;

	public long GetScore()
	{
		return score;
	}

	public int GetMultiplier()
	{
		return multiplier;
	}

	public void AddScore(int scoreToAdd, bool overrideMultiplier = false)
	{
		score += (overrideMultiplier) ? scoreToAdd : scoreToAdd * multiplier;
		scoreText.text = score.ToString();
	}

	public void SetMultiplier(int newMultipler)
	{
		multiplier = newMultipler;
		multiplierText.text = "x" + multiplier.ToString();
	}

	public void IncrementMultiplier()
	{
		if (Debug.isDebugBuild) {
			Debug.Log ("incrementmultipler");
		}
		multiplier++;
		multiplierText.text = "x" + multiplier.ToString();
	}
}
