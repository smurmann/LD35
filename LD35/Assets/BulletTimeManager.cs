﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BulletTimeManager : MonoBehaviour {
	public float bulletTimeDuration;
	public Text powerupTime;

	void Update ()
	{
		if (bulletTimeDuration < 0f)
		{
			Time.timeScale = 1f;
            AudioManager.instance.SetBGMSpeed(1.0f);
			powerupTime.text = "";
		}
		else
		{
			bulletTimeDuration -= Time.deltaTime;

			if (bulletTimeDuration > 0f)
			{
				powerupTime.text = bulletTimeDuration.ToString("F1");
			}
		}
	}

    public void SlowTime()
    {
        AudioManager.instance.SetBGMSpeed(0.8f);
        Time.timeScale = 0.5f;
        bulletTimeDuration = 2.5f;
    }
}
