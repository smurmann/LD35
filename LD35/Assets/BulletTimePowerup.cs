﻿using UnityEngine;
using System.Collections;

public class BulletTimePowerup : MonoBehaviour {
	public float slowedTimeScale = 0.5f;

	// Remember to multiply this by slowedTime to get real duration.
	public float effectiveTime = 5f;

	BulletTimeManager bulletTimeManager;

	void Start()
	{
		bulletTimeManager = FindObjectOfType<BulletTimeManager>();
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.CompareTag("Player"))
		{
			if (Debug.isDebugBuild) {
				Debug.Log("trigger enter");
			}

			SlowTime();
			Destroy(gameObject);
		}
	}

	void SlowTime()
	{
		Time.timeScale = slowedTimeScale;
		bulletTimeManager.bulletTimeDuration = effectiveTime;
	}
}
