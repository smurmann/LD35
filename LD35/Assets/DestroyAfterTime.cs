﻿using UnityEngine;
using System.Collections;

public class DestroyAfterTime : MonoBehaviour {

    float timeToDestroy = 5f;
    void OnEnable()
    {
        timeToDestroy = 5;
    }

    void Update()
    {
        timeToDestroy -= Time.deltaTime;

        if (timeToDestroy <= 0)
            ObjectPooling.instance.PoolObject(this.gameObject);
    }
}
