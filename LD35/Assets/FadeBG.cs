﻿using UnityEngine;
using System.Collections;

public class FadeBG : MonoBehaviour
{

    SpriteRenderer rend;

    // Use this for initialization
    void Start()
    {
        rend = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        var currColor = rend.color;
        currColor = new Color(1, 1, 1, currColor.a - Time.deltaTime/50);
        rend.color = currColor;
    }
}
