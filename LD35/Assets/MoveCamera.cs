﻿using UnityEngine;
using System.Collections;

public class MoveCamera : MonoBehaviour {

    float moveSpeed = 1;	
	// Update is called once per frame
	void Update () {
        Vector3 forceDir = new Vector2(0, -1);
        transform.position -= forceDir * Time.deltaTime * moveSpeed;

    }
}
