﻿using UnityEngine;
using System.Collections;

//[RequireComponent (typeof(CharacterController))]
public class Player : MonoBehaviour {
	public float moveForce = 100f;
	public float jumpSpeed = 10f;
	public float releaseJumpVelocity = 0f;
	public float raycastOffset;
	public float maxHorizontalSpeed = 10f;
	public float groundRaycastDistance = 10f;

	public bool sliding = false;

	Rigidbody2D myRigidbody;
	CharacterController controller;

	private Transform _transform;
	protected Transform MyTransform
	{
		get
		{
			if (_transform == null)
			{
				_transform = transform;
			}
			
			return _transform;
		}
	}

    public bool isStarted = false;

	void Start () {
		myRigidbody = GetComponent(typeof(Rigidbody2D)) as Rigidbody2D;
        isStarted = false;
    }

	void Update()
	{
        Debug.DrawRay(MyTransform.localPosition, Vector3.down * groundRaycastDistance, Color.white);

		if (Input.GetButtonDown("Jump") && isGrounded())
		{
			myRigidbody.velocity = new Vector2(myRigidbody.velocity.x, jumpSpeed);
		}
		else if (Input.GetButtonUp("Jump") && myRigidbody.velocity.y > releaseJumpVelocity)
		{
			myRigidbody.velocity = new Vector2(myRigidbody.velocity.x, releaseJumpVelocity);
		}
	}

	void FixedUpdate()
	{
		Vector2 velocity = myRigidbody.velocity;
		if (Input.GetButton("Left") && velocity.x > -maxHorizontalSpeed)
		{
			if (!sliding && velocity.x > 0)
			{
				myRigidbody.velocity = new Vector2(0f, velocity.y);
			}

			myRigidbody.AddForce(Vector2.left * moveForce);
			if (Debug.isDebugBuild) {
				Debug.Log("left");
			}
		}
		else if (Input.GetButton("Right") && velocity.x < maxHorizontalSpeed)
		{
			if (!sliding && velocity.x < 0)
			{
				myRigidbody.velocity = new Vector2(0f, velocity.y);
			}

			myRigidbody.AddForce(Vector2.right * moveForce);
			if (Debug.isDebugBuild) {
				Debug.Log("right");
			}
		}
	}

	bool isGrounded()
	{
		RaycastHit2D hit = Physics2D.Raycast(MyTransform.localPosition, Vector2.down, groundRaycastDistance);
		if (Debug.isDebugBuild && hit.collider) {
			Debug.Log("raycast hit " + hit.collider.gameObject.name);
		}
		return (hit.collider != null);
	}
}
