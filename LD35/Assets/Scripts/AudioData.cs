﻿using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class AudioData : ScriptableObject
{
    public AudioSet[] FX;
    public AudioClip[] Gobble;
    public AudioSet[] bgm;

}

[System.Serializable]
public class AudioSet
{
    public AudioClip sound;
    public string name;
}