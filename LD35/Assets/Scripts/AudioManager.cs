﻿using UnityEngine;
using System.Collections;

public class AudioManager : MonoBehaviour
{

    private static AudioManager _instance;
    public static AudioManager instance
    {
        get
        {
            if (_instance == null)
            {
                if (GameObject.FindObjectOfType<AudioManager>() != null)
                    _instance = GameObject.FindObjectOfType<AudioManager>();
                else
                {
                    GameObject audioManager = new GameObject("AudioManager");
                    audioManager.AddComponent<AudioManager>();

                }

            }
            return _instance;

        }
    }

    public AudioData audioData;

    public AudioSource[] EffectSources;
    public AudioSource BGMSources;

    public bool BGMOn = true;
    public bool FXOn = true;


    void Awake()
    {
        if (_instance != null)
            Destroy(this.gameObject);

        DontDestroyOnLoad(this);
        _instance = this;
    }

    void Start()
    {
        PlayBMG("bgm");
    }

    public void PlayFX(string clipname)
    {
        if (!FXOn)
            return;

        for (int i = 0; i < audioData.FX.Length; i++)
        {
            if (clipname == audioData.FX[i].name)
            {
                foreach (AudioSource source in EffectSources)
                {
                    if (source.isPlaying == false)
                    {
                        source.clip = audioData.FX[i].sound;
                        source.Play();
                        return;
                    }
                }
            }

        }
        Debug.LogError("Sound " + clipname + "not found.");
    }

    public void PlayFX(AudioClip clip)
    {
        foreach (AudioSource source in EffectSources)
        {
            if (source.isPlaying == false)
            {
                source.clip = clip;
                source.Play();
                return;
            }
        }
    }

    public void SetBGMSpeed(float value)
    {
        BGMSources.pitch = value;
    }

    public void PlayGobble()
    {
        var randNum = Random.Range(0, audioData.Gobble.Length);

        PlayFX(audioData.Gobble[randNum]);
    }


    public void PlayBMG(string clipname)
    {
        if (!BGMOn)
            return;

        BGMSources.loop = true;

        for (int i = 0; i < audioData.bgm.Length; i++)
        {
            if (clipname == audioData.bgm[i].name)
            {
                BGMSources.clip = audioData.bgm[i].sound;
                BGMSources.Play();
                return;
            }
        }
        Debug.LogError("Sound " + clipname + "not found.");
    }

    public void StopAll()
    {
        foreach (AudioSource source in EffectSources)
        {
            source.Stop();
        }
        BGMSources.Stop(); ;
    }


}