﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EndScreenManager : MonoBehaviour
{
	public Text scoreText;
	public static EndScreenManager instance;

	void Awake()
	{
		instance = this;
	}

	void OnEnable()
	{
		scoreText.text = "High score: " + ScoreManager.instance.HighScore();

		if (PlayerPrefs.GetInt("prevhighscore") > ScoreManager.instance.HighScore())
		{
			AudioManager.instance.PlayFX("highscore");
			scoreText.text += "\n New high score!";
		}

		scoreText.text += "\nEnter to retry.\n\n Brought to you by Victor Low, Sean-Li Murmann and Alex Hew";

		PlayerPrefs.SetInt("prevhighscore", ScoreManager.instance.HighScore());
		PlayerPrefs.Save();
	}

	void Update()
	{
		if (Input.GetKey(KeyCode.Return))
		{
			UnityEngine.SceneManagement.SceneManager.LoadScene(1, UnityEngine.SceneManagement.LoadSceneMode.Single);
			UnityEngine.SceneManagement.SceneManager.LoadScene(2, UnityEngine.SceneManagement.LoadSceneMode.Additive);
			UnityEngine.SceneManagement.SceneManager.LoadScene(3, UnityEngine.SceneManagement.LoadSceneMode.Additive);
			UnityEngine.SceneManagement.SceneManager.LoadScene(4, UnityEngine.SceneManagement.LoadSceneMode.Additive);
		}
	}
}
