﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
public class Enemy : MonoBehaviour
{
    public float moveSpeed;

    public ShapeState state;
    [HideInInspector]
    public bool isAlive;
    private WalkDir dir;

    public Sprite[] faces;

    SpriteRenderer spriteRenderer;

    void OnEnable()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();

        int rand = Random.Range(0, 2);
        if (rand == 0)
            dir = WalkDir.Left;
        else
            dir = WalkDir.Right;

        state = (ShapeState)Random.Range(1, 4);
        UpdateVisual();
        isAlive = true;
    }

    void UpdateVisual()
    {
        if (gameObject.tag == "TimePowerUp")
            return;
        switch(state)
        {
            case ShapeState.Circle:
                spriteRenderer.sprite = faces[0];
                break;
            case ShapeState.Square:
                spriteRenderer.sprite = faces[1];
                break;
            case ShapeState.Triangle:
                spriteRenderer.sprite = faces[2];
                break;
        }
    }

    void OnDisable()
    {
        isAlive = false;
        dir = WalkDir.None;
        state = ShapeState.None;
    }

    void Update()
    {
        Vector3 forceDir = new Vector2((int)dir, 0);
        transform.position += forceDir * Time.deltaTime * moveSpeed;
    }

    void ReverseDirection()
    {
        Debug.Log("Change dir");
        if (dir == WalkDir.Left)
            dir = WalkDir.Right;
        else
            dir = WalkDir.Left;
    }


    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Wall")
            ReverseDirection();
    }
}

public enum ShapeState
{
    None = 0,
    Square,
    Triangle,
    Circle
}

public enum WalkDir
{
    None = 0,
    Right = 1,
    Left = -1
}
