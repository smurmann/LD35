﻿using UnityEngine;
using System.Collections;

public class EnemyDestroyer : MonoBehaviour
{
    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Enemy")
            ObjectPooling.instance.PoolObject(collision.gameObject);
        else if (collision.gameObject.tag == "Player")
            collision.gameObject.GetComponent<PlayerShape>().OnKillPlayer();
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Floor")
            collision.GetComponent<Floor>().SpawnNewFloor();
    }
}
