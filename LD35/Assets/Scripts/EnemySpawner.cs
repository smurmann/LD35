﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour
{

    float time = 2;
    float timer;

    // Update is called once per frame
    void Update()
    {
        if (!GameManager.instance.isStarted)
            return;
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            ObjectPooling.instance.GetObject("Enemy", transform);

            var newTime = time - ScoreManager.instance.GetScore() * 0.05f;

            if (newTime < 0.75f)
                newTime = 0.75f;

            if(ScoreManager.instance.GetScore() % 10 >= 1)
            {
                newTime -= (ScoreManager.instance.GetScore() % 5) * 0.025f;
            }

            if (newTime < 0.2f)
                newTime = 0.2f;

            timer = newTime;
        }
    }

    public void SpawnTimeSlow()
    {
        ObjectPooling.instance.GetObject("TimePowerUp", transform);
    }
}
