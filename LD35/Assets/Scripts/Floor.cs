﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BoxCollider2D))]
public class Floor : MonoBehaviour
{
    private static string[] FloorNames = {"Floor 1.0", "Floor 2.0",
                                            "Floor 3.0", "Floor 3.1",
                                            "Floor 4.0", "Floor 4.1",
                                            "Floor 5", "Floor 5.1", "Floor 6"};


    public void SpawnNewFloor()
    {
		if (!EndScreenManager.instance.enabled)
		{
			ScoreManager.instance.AddScore(1);
		}

        ObjectPooling.instance.GetObject(FloorNames[Random.Range(0, FloorNames.GetLength(0))],new Vector3(0, Camera.main.transform.position.y + 8.41f,0));
        ObjectPooling.instance.PoolObject(this.gameObject);
    }
}
