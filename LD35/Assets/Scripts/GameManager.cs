﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

    public static GameManager instance;

    public bool isStarted = false;
	
    void Awake()
    {
        instance = this;
    }
	// Update is called once per frame
	void Update () {

        if (Input.GetButton("Right"))
        {
            isStarted = true;
        }
        else if (Input.GetButton("Left"))
        {
            isStarted = true;
        }
        else if (Input.GetButton("Jump"))
        {
            isStarted = true;
        }
    }
}
