﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[ExecuteInEditMode]
[System.Serializable]
public class ObjectPooling : MonoBehaviour
{
    public bool intialized = false;
    public bool isEditorMode
    {
        get
        {
            if (!Application.isPlaying)
                return true;
            return false;
        }
    }
    //used for singleton instance
    private static ObjectPooling _instance;
    public static ObjectPooling instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<ObjectPooling>();
            }
            return _instance;
        }

    }
    [SerializeField]
    public ObjectPoolingData ObjectPoolData;

    /// <summary>
    /// The object prefabs which the pool can handle.
    /// </summary>
    [SerializeField]
    private PooledObject[] objectPrefabs;

    /// <summary>
    /// The pools that hold the objects currently available.
    /// </summary>
    [SerializeField]
    private List<GameObject>[] objectPools;

    [SerializeField]

    private GameObject[] pools;

    /// <summary>
    /// Container to Hold pools.
    /// </summary>
    [SerializeField]
    private GameObject poolHolder;

    void Awake()
    {
        _instance = this;
        InitializePool();

        StartCoroutine(checkPoolSizes());
    }

    IEnumerator checkPoolSizes()
    {
        while (true)
        {
            for (int i = 0; i < objectPrefabs.GetLength(0); i++)
            {
                if (objectPools[i].Count < objectPrefabs[i].initialPoolSize)
                {
                    GameObject newObj = Instantiate(objectPrefabs[i].Prefab) as GameObject;
                    newObj.name = objectPrefabs[i].Prefab.name;
                    PoolObject(newObj);
                }
            }
            yield return new WaitForEndOfFrame();
        }


    }

    private void InitPoolHolder()
    {
        if (transform.childCount > 0)
        {
            DestroyImmediate(transform.GetChild(0).gameObject);
        }
        poolHolder = new GameObject("Pools");
        poolHolder.transform.parent = this.transform;
    }

    private void InitPoolData()
    {
        objectPrefabs = ObjectPoolData.objectPrefabs;
    }


    private void InitializePools()
    {
        objectPools = new List<GameObject>[objectPrefabs.Length];
        pools = new GameObject[objectPrefabs.Length];

        for (int i = 0; i < objectPrefabs.Length; i++)
        {
            var prefabClass = objectPrefabs[i];

            GameObject newPool = new GameObject(prefabClass.Prefab.name + "_Pool");
            newPool.transform.parent = poolHolder.transform;
            pools[i] = newPool;

            objectPools[i] = new List<GameObject>();

            int bufferAmount = prefabClass.initialPoolSize;

            for (int n = 0; n < bufferAmount; n++)
            {
                GameObject newObj = Instantiate(prefabClass.Prefab) as GameObject;
                newObj.name = prefabClass.Prefab.name;
                PoolObject(newObj);
            }
        }

        intialized = true;
    }

    /// <summary>
    /// Send Object back to Pool.
    /// </summary>
    public void PoolObject(GameObject obj)
    {
        for (int i = 0; i < objectPrefabs.Length; i++)
        {
            if (objectPrefabs[i].Prefab.name.ToLower() == obj.name.ToLower())
            {
                obj.SetActive(false);
                obj.transform.SetParent(pools[i].transform);
                obj.transform.position = new Vector3(-50, -50, -50);
                obj.transform.eulerAngles = new Vector3(0, 0, 0);
                objectPools[i].Add(obj);
                return;
            }
        }

        Destroy(obj);
    }
    /// <summary>
    /// Set as child of a transform and reset scale, rotation and position
    /// </summary>
    public GameObject GetObject(string id, Transform transform)
    {
        var temp = GetObject(id, Vector3.zero, Vector3.zero);
        temp.transform.SetParent(transform, false);
        temp.transform.localPosition = Vector3.zero;
        temp.transform.localEulerAngles = Vector3.zero;
        temp.transform.localScale = Vector3.one;
        return temp;
    }

    public GameObject GetObject(string id, Vector3 pos, Vector3 rotation)
    {
        for (int i = 0; i < ObjectPoolData.objectPrefabs.Length; i++)
        {
            PooledObject tempObj = ObjectPoolData.objectPrefabs[i];
            if (tempObj.Prefab.name.ToLower() == id.ToLower())
            {
                if (isEditorMode)
                {
                    GameObject pooledObject = Instantiate(tempObj.Prefab) as GameObject;
                    pooledObject.transform.position = pos;
                    pooledObject.transform.eulerAngles = rotation;
                    pooledObject.name = pooledObject.name.Replace("(Clone)", "");
                    return pooledObject;
                }

                if (objectPools[i].Count > 0)
                {
                    GameObject pooledObject = objectPools[i][0];
                    objectPools[i].RemoveAt(0);
                    pooledObject.transform.SetParent(null, false);
                    pooledObject.SetActive(true);
                    pooledObject.transform.position = pos;
                    pooledObject.transform.eulerAngles = rotation;
                    return pooledObject;
                }
                else
                {
                    GameObject pooledObject = Instantiate(tempObj.Prefab) as GameObject;
                    pooledObject.transform.position = pos;
                    pooledObject.transform.eulerAngles = rotation;
                    pooledObject.name = pooledObject.name.Replace("(Clone)", "");
                    return pooledObject;
                }
            }
        }
        Debug.Log("Object:" + id + " not found!");
        return null;
    }

    public GameObject GetObject(string id, Vector3 pos)
    {
        for (int i = 0; i < ObjectPoolData.objectPrefabs.Length; i++)
        {
            PooledObject tempObj = ObjectPoolData.objectPrefabs[i];
            if (tempObj.Prefab.name.ToLower() == id.ToLower())
            {
                if (isEditorMode)
                {
                    GameObject pooledObject = Instantiate(tempObj.Prefab) as GameObject;
                    pooledObject.transform.position = pos;
                    //pooledObject.transform.eulerAngles = rotation;
                    pooledObject.name = pooledObject.name.Replace("(Clone)", "");
                    return pooledObject;
                }

                if (objectPools[i].Count > 0)
                {
                    GameObject pooledObject = objectPools[i][0];
                    objectPools[i].RemoveAt(0);
                    pooledObject.transform.SetParent(null, false);
                    pooledObject.SetActive(true);
                    pooledObject.transform.position = pos;
                    //pooledObject.transform.eulerAngles = rotation;
                    return pooledObject;
                }
                else
                {
                    GameObject pooledObject = Instantiate(tempObj.Prefab) as GameObject;
                    pooledObject.transform.position = pos;
                    //pooledObject.transform.eulerAngles = rotation;
                    pooledObject.name = pooledObject.name.Replace("(Clone)", "");
                    return pooledObject;
                }
            }
        }
        Debug.Log("Object:" + id + " not found!");
        return null;
    }

    /// <summary>
    /// Instantiate prefab at it own position and rotation
    /// </summary>
    /// <param name="id"></param>
    /// <param name="pos"></param>
    /// <param name="rotation"></param>
    /// <returns></returns>
    public GameObject GetObject(string id)
    {
        for (int i = 0; i < objectPrefabs.Length; i++)
        {
            PooledObject tempObj = objectPrefabs[i];
            if (tempObj.Prefab.name.ToLower() == id.ToLower())
            {
                return GetObject(id, tempObj.Prefab.transform.position, tempObj.Prefab.transform.eulerAngles);
            }
        }

        Debug.Log("Object:" + id.ToString() + " not found!");
        return null;
    }

    public string getName(GameObject obj)
    {
        for (int i = 0; i < objectPrefabs.Length; i++)
        {
            PooledObject tempObj = objectPrefabs[i];
            if (tempObj.Prefab.name.ToLower() == obj.name.ToLower())
            {
                return tempObj.Prefab.name;
            }
        }
        Debug.LogError("No Gameobject of name:" + obj.name + " found");
        return "NULL";
    }

    public void InitializePool()
    {
        if (_instance == null)
            _instance = this;

        InitPoolData();
        InitPoolHolder();
        InitializePools();
    }
}
