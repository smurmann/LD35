﻿using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class ObjectPoolingData : ScriptableObject
{
    public PooledObject[] objectPrefabs;
}


[System.Serializable]
public class PooledObject
{
    public GameObject Prefab;
    public int initialPoolSize;
}