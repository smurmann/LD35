﻿using UnityEngine;
using System.Collections;


public class PlayerShape : MonoBehaviour
{

    public ShapeState state = ShapeState.None;
    public Sprite[] faces;

    SpriteRenderer spriteRenderer;
	ParticleSystem deathExplosion;
	CameraShake cameraShake;

    void Start()
    {
		deathExplosion = GetComponentInChildren<ParticleSystem>();
        spriteRenderer = GetComponent<SpriteRenderer>();
		cameraShake = FindObjectOfType<CameraShake>();
    }

    public void OnKillEnemy(GameObject enemy)
    {
        Debug.Log("Kill Enemy");
        ObjectPooling.instance.PoolObject(enemy);
        AudioManager.instance.PlayGobble();
        ScoreManager.instance.AddScore(1);
        ObjectPooling.instance.GetObject("Explosion Particles2",transform.position);
    }

    public void OnKillPlayer()
    {
        Debug.Log("Kill Player");
        AudioManager.instance.PlayFX("explosion");

		spriteRenderer.enabled = false;
		PlayerController player = GetComponent(typeof(PlayerController)) as PlayerController;
		player.enabled = false;
		deathExplosion.Play();
		Collider2D collider = GetComponent(typeof(Collider2D)) as Collider2D;
		collider.enabled = false;

//		FindObjectOfType<MoveCamera>().enabled = false;
		cameraShake.enabled = true;
		cameraShake.shakeDuration = 0.7f;

		EndScreenManager.instance.enabled = true;

		Destroy(this);
    }

    void UpdateVisuals()
    {
        switch (state)
        {
            case ShapeState.Circle:
                spriteRenderer.sprite = faces[0];
                break;
            case ShapeState.Square:
                spriteRenderer.sprite = faces[1];
                break;
            case ShapeState.Triangle:
                spriteRenderer.sprite = faces[2];
                break;
        }
    }

    void Update()
    {
        if (Input.GetButton("Right"))
        {
            state = ShapeState.Square;
            UpdateVisuals();
        }
        else if (Input.GetButton("Left"))
        {
            state = ShapeState.Circle;
            UpdateVisuals();
        }
        else if (Input.GetButton("Jump"))
        {
            state = ShapeState.Triangle;
            UpdateVisuals();
        }
    }
}
