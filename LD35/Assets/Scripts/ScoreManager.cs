﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    public static ScoreManager instance;

    void Awake()
    {
        instance = this;
        score = 0;
        AddScore(0);
    }

    public Text scoreText;

    long score;

    public long GetScore()
    {
        return score;
    }

	public void ResetHighScore()
	{
		PlayerPrefs.DeleteAll();
	}

	public int HighScore()
	{
		return PlayerPrefs.GetInt("scorekey");
	}

    public void AddScore(int scoreToAdd)//, bool overrideMultiplier = false)
    {
        //score += (overrideMultiplier) ? scoreToAdd : scoreToAdd * multiplier;
        score += scoreToAdd;
        scoreText.text = score.ToString();

        if(score % 10 == 0 && score > 0)
        {
            FindObjectOfType<EnemySpawner>().SpawnTimeSlow();
        }

		if (score > HighScore())
		{
			PlayerPrefs.SetInt("scorekey", (int)score);
			PlayerPrefs.Save();
		}

        //AudioManager.instance.SetBGMSpeed(1 + (int)score/5 * 0.05f);
    }
}
