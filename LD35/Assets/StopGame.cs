﻿using UnityEngine;
using System.Collections;

public class StopGame : MonoBehaviour
{

    void Awake()
    {
        DontDestroyOnLoad(this);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();
    }
}
